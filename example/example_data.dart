import 'package:ana_metadata/metadata.dart';

part 'example_data.g.dart';

abstract class Car implements Propsy, Built<Car, CarBuilder> {
  factory Car([updates(CarBuilder b)]) = _$Car;
  Car._();
}

Schema generateSchemaForCar() {
  return Schema((builder) => builder
    ..id = 'not important here'
    ..rules['Name'] = Rule(RuleType.text, 32, false, false, 0)
    ..rules['Manufacturer'] = Rule(RuleType.text, 64, false, false, 1)
    ..rules['Review'] = Rule(RuleType.text, 1024, false, false, 2)
    ..rules['Door'] = Rule(RuleType.number, 0, false, false, 3)
    ..rules['Bio Fuel'] = Rule(RuleType.toggle, 0, false, false, 4)
    );
}

Car generateCar() {
  return Car((builder) => builder
    ..id = 'not important too'
    ..props['Name'] = JsonObject('Model 3')
    ..props['Manufacturer'] = JsonObject('Tesla')
    ..props['Review'] = JsonObject('''Highs: Quick acceleration and nimble handling, real-world-usable driving range, lots of future-is-now technology.
Lows: Driving range doesn't hold up on the highway, stark interior layout.
Verdict: Similar driving range and semi-autonomous driving tech but at half the price of the Model S. ''')
    ..props['Door'] = JsonObject(4)
    ..props['Bio Fuel'] = JsonObject(false)
    );
}
