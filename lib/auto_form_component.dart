import 'dart:async' show StreamController;

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_components/focus/focus.dart';
import 'package:angular_components/material_input/material_auto_suggest_input.dart';
import 'package:angular_components/material_input/material_input.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_toggle/material_toggle.dart';
import 'package:angular_components/material_input/material_number_accessor.dart';

import 'auto_form_entry.dart';
export 'auto_form_entry.dart';

@Component(
    selector: 'auto-form',
    directives: [
      AutoFocusDirective,
      MaterialAutoSuggestInputComponent,
      MaterialButtonComponent,
      materialInputDirectives,
      materialNumberInputDirectives,
      MaterialToggleComponent,
      NgClass,
      NgControlName,
      NgFor,
      NgFormModel,
      NgIf,
      NgSwitch,
      NgSwitchWhen,
    ],
    exports: [RuleType],
    styleUrls: [
      'style/mdc-layout-grid/mdc-layout-grid.scss.css',
      'auto_form_component.scss.css'
    ],
    templateUrl: 'auto_form_component.html')
class AutoFormComponent implements OnChanges {
  @Input()
  List<AutoFormEntry> entries;
  @Input()
  String submitButtonLabel = "Apply";
  @Input()
  String cancelButtonLabel = "Cancel";
  @Input()
  String deleteButtonLabel = "DELETE";
  @Input()
  bool cancelAlwaysOn = false;
  @Input()
  bool canBeDelete = false;
  @Input()
  bool readonly = false;
  @Input()
  bool explicitDirty = false;
  @Input()
  bool submitUnchanged = false;

  final _changeSubmition = StreamController<Map<String, dynamic>>();
  final _cancellation = StreamController();
  final _deletion = StreamController();
  final _changes = StreamController<Map<String, dynamic>>();

  @Output()
  Stream get changeSubmitted => _changeSubmition.stream;
  @Output()
  Stream get cancelled => _cancellation.stream;
  @Output()
  Stream get deleted => _deletion.stream;
  @Output()
  Stream get changes => _changes.stream;

  ControlGroup forming;
  
  bool get isChanged => forming.dirty || explicitDirty;
  bool get shouldShowAction => !readonly && !_changes.hasListener;

  AutoFormComponent();

  @override
  void ngOnChanges(Map<String, SimpleChange> changes) {
    if (changes.containsKey('entries')) {
      if (entries == null) {
        return;
      }

      var entryMap = Map<String, AbstractControl>();
      entries.forEach((entry) {
        entryMap[entry.id] = Control(entry.originalValue);
      });
      forming = ControlGroup(entryMap);

      if (_changes.hasListener) {
        forming.valueChanges.listen((changes) {
          if (forming.dirty) {
            _changes.add(changes);
          } else {
            _changes.add(null);
          }
        });
      }
    }
  }

  Map<String, bool> classFor(AutoFormEntry entry) {
    if (entry.rule.ruleType == RuleType.text) {
      if (entry.rule.characterLimit <= 16) {
        return {'mdc-layout-grid__cell--span-2': true};
      } else if (entry.rule.characterLimit <= 64) {
        return {'mdc-layout-grid__cell--span-4': true};
      } else {
        return {'mdc-layout-grid__cell--span-6': true};
      }
    }
    return {'mdc-layout-grid__cell--span-2': true};
  }

  void onSubmit() {
    var changes = Map<String, dynamic>();
    forming.controls.forEach((id, c) {
      if (submitUnchanged || c.dirty) {
        print('$id is dirty. it is  now ${c.value}');
        changes[id] = c.value;
      }
    });
    if (changes.isNotEmpty || explicitDirty) {
      _changeSubmition.add(changes);
    }
  }

  void onCancel() {
    _cancellation.add(null);
  }

  void onDelete() {
    _deletion.add(null);
  }
}
