import 'package:ana_metadata/metadata.dart';
export 'package:ana_metadata/metadata.dart';

class AutoFormEntry {
  String id;
  Rule rule;
  dynamic originalValue;
  List<String> suggestions;
  bool disabled = false;

  String toString() {
    return '$id $originalValue $rule';
  }
}