# Automatic Generated Form

Power all the forms in ANA. Due to the undefined layout of user data (the reason ANA has ANA_metadata), we cannot hand craft beautiful forms. We always assume we do not know all the possible fields for each type of data.

So we use the `Rule` from ANA_metadata to help determine the type and size of each input, e.g. character limit to determine the size of the text box, should it be a toggle or drop down. Then we lay them down in a grid and hopefully they look good in all kind of combinations.

## Auto Form Component

`AutoFormComponent` take a list of `AutoFormEntry' as input and output the changes when button are clicked.

It does not care how's the original data looks like, it can be a class, or a aggregated list of entries from multiple document. It just take care of the list of entries pass to it.

## Auto Form Entry

`AutoFormEntry` carries the context and value for each field of a data. These entries are the only way to use `AutoFormComponent`. We try not to introduce more input.

## CSS

We copied Google's `mdc-layout-grid/mdc-layout-grid.css` style to lay down the grid of inputs. I am indifferent of it. But has a feeling we might be better off if we able to roll our own style.

## TODO

- Introduce a new `Rule` that make a field a dropdown instead of text field with suggestion. This is for "these are the only possible value" kind of situation.
- Some combination of entries are just outright ugly, e.g. 3 medium length follow by a long one, left blank at the second row. What can we do?
- Error checking and prevent submission is non-existent.
- The button's color class is missing in this project, should we get them here? Or roll our own style with mixin?
- Do we need test?
